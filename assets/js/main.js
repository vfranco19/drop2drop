function modal_warning(msg){
  swal({
    title: '<i style="color:#ffa000;" class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>',
    text: msg,
    html: true
  });
}
//modal de operacion exitosa
function modal_success(){
    swal({
      title: "ÉXITO!",
      text: "Operación Exitosa!",
      type: "success",
      timer: 1000,
      showConfirmButton: false
    });
}
//modal de error
function modal_error(msg){
    if(Array.isArray(msg[0]))
    {
        var tmp='';
        for (var i = 0; i < msg[0].length; i++)
        {
            if(tmp!='')
            {
                tmp += '<br>'+msg[0][i];
            }
            else
            {
                tmp += msg[0][i];
            }
        }
        msg=tmp;
    }
    swal({
      title: "ERROR!",
      text: msg,
      type: "error",
      html: true,
      showConfirmButton: true
    });
}
function modal_other(msg){
    swal({
      title: "ÉXITO!",
      text: msg,
      type: "success",
      timer: 2500,
      showConfirmButton: true
    });
}
function modal_close(){
    swal.close();
    $(':input[type="submit"]').prop('disabled', false);
}
function logout(id,ruta)
{
  console.log(id,ruta);
  swal({
    title: "CONFIRMAR",
    text: "Esta seguro de realizar la operación?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#2A67BD",
    confirmButtonText: "Confirmar",
    cancelButtonText: "Cancelar",
    closeOnConfirm: false,
    closeOnCancel: false,
    showLoaderOnConfirm: true
  },
  function(isConfirm)
  {
    if(isConfirm)
    {
        $.ajax({
            url: '/api/Autenticar/logout',
            type: 'post',
            headers:{'token':$("#token").val()},
            data: {
                idUser:id,
            },
        })
        .done(function(resp)
        {
            if( resp.ResponseCode)
            {
                //modal_success();
                if(ruta=='admin')
                {
                  location='/loginAdmin';
                }
                else
                {
                  location='/login';
                }
            }else
            {
                modal_error(resp.ResponseMessage);
            }
        })
        .fail(function(jqXHR, textStatus, errorThrow){
            if(jqXHR.status===500)
            {
                modal_error("ERROR INTERNO DEL SERVIDOR");
            }                 
            if(jqXHR.status===401)
            {
                ver_logout();
            }
            if(jqXHR.status===404)
            {
                modal_error("RUTA NO ENCONTRADA (404 NOT FOUND)");
            }
        });
    }
    else
    {
        modal_close();
    }
  });    
}