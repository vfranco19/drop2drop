/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
    '/': {
        view: 'pages/homepage',
        locals: {
            layout: false,
        }
    },    
    '/login': {
        view: 'login',
        locals: {
            layout: false
        }
    },
    '/loginAdmin': {
        view: 'loginAdmin',
        locals: {
            layout: false
        }
    }, 
    '/password': {
        view: 'password',
        locals: {
            layout: false
        }
    },        
    '/login': 
    {
        controller:'AutenticarController', action:'loginUserView'
    },
 
    '/listAdmins': 
    {
        controller:'UsuariosController', action:'listAdmins'
    },    
    //RUTAS USUARIOS
    'post /api/Usuarios/store': 
    {
        controller:'UsuariosController', action:'store'
    },
    //FIN RUTAS USUARIOS
    //RUTAS AUTENTICAR
    'post /api/Autenticar/sigin': 
    {
        controller:'AutenticarController', action:'sigin'
    },
    'post /api/Autenticar/resetPassword': 
    {
        controller:'AutenticarController', action:'resetPassword'
    },
    'post /api/Autenticar/logout': 
    {
        controller:'AutenticarController', action:'logout'
    },      
    //FIN RUTAS AUTENTICAR

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
