/**
 * Usuarios.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    fullName : 'string',
    typeDoc : {
      model: 'typeDoc',
    },
    numDoc : 'string',
    numPhone : 'string',
    address : 'string',
    email : 'string',
    password : 'string',
    state : {
      model : 'States',
    },
    city : {
      model : 'Cities',
    },
    idRol : 'string',
    codeRecovery : 'string'
  },
  generarPassword:function(longitud)
  {
    var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ2346789";
    var contraseña = "";
    for (i=0; i<longitud; i++) contraseña += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
    return contraseña;
  },
};

