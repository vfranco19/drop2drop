/**
 * Mensajes.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var msg = [];
var men = new Object();
module.exports = {

	attributes: {
		id: {
		    type: 'string',
		    columnName: '_id'
		},
	},
	Mensaje:function(n, data = [])
	{
		msg[10000] = {num:1, text:'Operación realizada con éxito'};
		msg[10003] = {num:2, text:'Sin error pero no procesa'};

        msg[20000] = {num:0, text:'Error al guardar los datos'};
        msg[20001] = {num:0, text:'El Correo ingresado ya se encuentra registrado'};
        msg[20002] = {num:0, text:'Usuario no registrado'};
        msg[20003] = {num:0, text:'Error en funcion'};
        msg[20004] = {num:0, text:'La contraseña ingresada es incorrecta'};
		
		msg[40000] = {num:0, text:data};
		if(data.length>0)
		{
			var datos = {
				'ResponseCode'		: msg[n].num,
				'ResponseMessage'	: msg[n].text,
				'ResponseData'		: data
			}
		}
		else
		{
			var datos = {
				'ResponseCode'		: msg[n].num,
				'ResponseMessage'	: msg[n].text
			}
		}
		return datos;
	}
};