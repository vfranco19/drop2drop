/**
 * AutenticarController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var bcrypt = require('bcrypt');
module.exports = {
  
    loginUserView:function(req, res)
    {
        if(req.session.authenticated)
        {
            return res.view('pages/users/index',{ iduser:req.session.User.id, name:req.session.User.fullName, email:req.session.User.email, idRol:req.session.User.idRol});
        }
        else
        {
            return res.view('login', {layout: false},function (err, html) 
            {
                return res.send(html);
            });
        }
    },
    /*
    loginAdminView:function(req, res)
    {
        if(req.session.authenticated)
        {
            return res.view('pages/admin/listAdmins',{layout: 'layouts/layoutAdmin', iduser:req.session.User[0].iduser, name:req.session.User[0].firstname+' '+req.session.User[0].lastname, email:req.session.User[0].email, img:req.session.User[0].image, token:req.session.User[0].token, idrol:req.session.User[0].idrol});
        }
        else
        {
            return res.view('loginAdmin', {layout: false},function (err, html) 
            {
                return res.send(html);
            });
        }
    },
    */
    sigin: async function(req,res)
    {
        try
        {
            var dataUser = await Usuarios.find({email:req.param('email'),idRol:req.param('idRol')});
            if(dataUser.length>0)
            {
                var match = await bcrypt.compare(req.param('password'), dataUser[0].password);
                if(match)
                {
                    delete dataUser[0].password;
                    req.session.authenticated = true;
                    req.session.User = dataUser[0];
                    return res.json(Mensajes.Mensaje(10000,{}));                     
                }
                else
                {
                    msj = Mensajes.Mensaje(20004,{});
                    return res.send(msj);
                }
                //console.log(match);
            }
            else
            {
                msj = Mensajes.Mensaje(20002,{});
                return res.send(msj); 
            }

        }
        catch(err)
        {
            console.log('err',err)
            msj = Mensajes.Mensaje(20003,{});
            return res.send(msj);             
        }
    },
    resetPassword: async function(req,res)
    {
        try
        {
            var data = await Usuarios.find({email:req.param('email')});
            if(data.length>0)
            {
                var newPass = Usuarios.generarPassword(8);
                var password = bcrypt.hashSync(newPass,10);
                await Usuarios.update({id:data[0].id},{password:password});
                Mailer.sendEmail('recoveryPassword', req.param("email"), 'Cambio de password de Drop2Drop', data[0].fullName, newPass);
                msj = Mensajes.Mensaje(10000,{});
            }
            else
            {
                msj = Mensajes.Mensaje(20002,{});
            }
            return res.send(msj);
        }
        catch(err)
        {
            console.log(err);
            msj = Mensajes.Mensaje(20003,{});
            return res.send(msj);
        }
    },
    logout: async function(req,res)
    {
        try
        {
			//var updated = await Usuarios.update({id:req.param("idUser")},{token:''});
			req.session.authenticated = false;
            req.session.destroy();
            msj = Mensajes.Mensaje(10000,{});
			return res.send(msj);  
        }
        catch(err)
        {
            console.log(err);
            msj = Mensajes.Mensaje(20003,{});
            return res.send(msj);
        }
    },
};

