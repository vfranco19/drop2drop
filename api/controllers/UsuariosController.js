/**
 * UsuariosController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var bcrypt = require('bcrypt');
module.exports = {
    listAdmins:function(req, res)
    {
        //console.log(req.session.authenticated, req.session.User);
        if(req.session.authenticated)
        {
            return res.view('pages/admin/listAdmins',{ layout:'layouts/layoutAdmin', iduser:req.session.User.id, name:req.session.User.fullName, email:req.session.User.email, idRol:req.session.User.idRol});
        }
        else
        {
            return res.view('loginAdmin', {layout: false},function (err, html) 
            {
                return res.send(html);
            });
        }
    },
    store:async function(req, res)
    {
        try
        {
            var password = bcrypt.hashSync(req.param('password'),10);            
            data =
            {
                fullName : req.param('fullName'),
                typeDoc : req.param('typeDoc'),
                numDoc : req.param('numDoc'),
                numPhone : req.param('numPhone'),
                adress : req.param('address'),
                email : req.param('email'),
                password : password,
                state : req.param('state'),
                city : req.param('city'),
                idRol : req.param('idRol')
            };
            var dataUser = await Usuarios.find({email:req.param('email')});
            if(dataUser.length==0)
            {
                await Usuarios.create(data);
                msj = Mensajes.Mensaje(10000,{});
                return res.send(msj);                
            }
            else
            {
                msj = Mensajes.Mensaje(20001,{});
                return res.send(msj);                 
            }
        }
        catch(err)
        {
            console.log('err',err)
            msj = Mensajes.Mensaje(20000,{});
            return res.send(msj);            
        }
    }, 
};

