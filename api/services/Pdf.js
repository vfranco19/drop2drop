module.exports.exportPdf = function(template, data, opt = '', nameDoc) 
{
	sails.hooks.pdf.make(
		template, 
		{
			data: data
		},
		{
			output: 'assets/fuecExoport/'+nameDoc+'.pdf'
		},
		function(err)
		{
			console.log(err || "ok PDF!!");
			if(!err)
			{
				return 'assets/fuecExoport/'+nameDoc+'.pdf';
			}
		}
	);
}