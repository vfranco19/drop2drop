module.exports.sendEmail = function(template, email, subject, name, pass) {
	sails.hooks.email.send(
	template, 
	{
		name: name,
		pass: pass,
		email: email,
	},
	{
		to: email,
		subject: subject
	},
	function(err) {console.log(err || "Mail Sent!");}
	)
}